insert\_cancel.vim
==================

This small plugin provides a mapping in insert mode to cancel the current
insert operation by undoing the last change upon insert exit, if a change to
the buffer was made during insert mode, while firing `InsertLeave`.  This is
intended for remapping Ctrl-C in insert mode to do something more useful.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
