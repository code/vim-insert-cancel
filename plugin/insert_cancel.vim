"
" insert_cancel.vim: Insert mode mapping to cancel the current insert
" operation by undoing the last change upon insert exit, if we made a change,
" while firing InsertLeave.  This is intended for remapping Ctrl-C in insert
" mode to do something more useful.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_insert_cancel') || &compatible || v:version < 700
  finish
endif
let loaded_insert_cancel = 1

" On entering a buffer, and then each time the cursor moves in normal mode,
" cache the current change number
augroup insert_cancel
  autocmd!
  autocmd BufEnter,CursorMoved *
        \ let b:insert_cancel_changenr = changenr()
augroup END

" Undo any changes if they exceed the cached number on insert map key press
inoremap <expr> <Plug>(InsertCancel)
      \ changenr() > b:insert_cancel_changenr ? "\<Esc>u" : "\<Esc>"
